import unittest
import xmlrunner

class TestExample(unittest.TestCase):
    def testIsTrue(self):
        self.assertTrue(True)

    def testIsFalse(self):
        self.assertFalse(False)

if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
